const fs = require('fs');
const path = require('path');
const Database = require('better-sqlite3');
const { ControllerMixinDatabase, ORM, KohanaJS } = require('kohanajs');
const { ORMAdapterSQLite, DatabaseDriverBetterSQLite3 } = require('@kohanajs/mod-database-adapter-better-sqlite3');

ORM.defaultAdapter = ORMAdapterSQLite;
ControllerMixinDatabase.DEFAULT_DATABASE_DRIVER = DatabaseDriverBetterSQLite3;

KohanaJS.init({ EXE_PATH: `${__dirname}/loginTest/test`, APP_PATH: `${__dirname}/loginTest/test` });
KohanaJS.initConfig(new Map([
  ['cookie', ''],
  ['session', ''],
  ['auth', require('../config/auth')],
  ['register', require('../config/register')],
  ['edm', ''],
]));

require('@kohanajs/mod-crypto');
require('@kohanajs/mod-session');

KohanaJS.classPath.set('model/IdentifierUser.js', require('../classes/model/IdentifierUser'));
KohanaJS.classPath.set('model/Person.js', require('../classes/model/Person'));
KohanaJS.classPath.set('model/Role.js', require('../classes/model/Role'));
KohanaJS.classPath.set('model/User.js', require('../classes/model/User'));
KohanaJS.classPath.set('model/Login.js', require('../classes/model/Login'));

const ControllerAuth = require('../classes/controller/Auth');

describe('login test', () => {
  // copy db
  const target = path.normalize(`${__dirname}/loginTest/db/user.sqlite`);
  if (fs.existsSync(target))fs.unlinkSync(target);
  fs.copyFileSync(path.normalize(`${__dirname}/loginTest/defaultDB/user.sqlite`), target);
  const db = new Database(target);

  const target2 = path.normalize(`${__dirname}/loginTest/db/session.sqlite`);
  if (fs.existsSync(target2))fs.unlinkSync(target2);
  fs.copyFileSync(path.normalize(`${__dirname}/loginTest/defaultDB/session.sqlite`), target2);
  const db2 = new Database(target2);

  beforeEach(() => {
    db.exec('INSERT INTO persons (id, first_name, last_name) VALUES (1, \'test\', \'\');');
    db.exec('INSERT INTO users (id, person_id) VALUES (1, 1);');
    db.exec('INSERT INTO user_roles (user_id, role_id) VALUES (1, 2)');

    db.exec('INSERT INTO persons (id, first_name, last_name) VALUES (2, \'peter\', \'\');');
    db.exec('INSERT INTO users (id, person_id) VALUES (2, 2);');
    db.exec('INSERT INTO user_roles (user_id, role_id) VALUES (2, 2)');
  });

  afterEach(() => {
    db.exec('DELETE FROM persons');
    db.exec('DELETE FROM users');
    db.exec('DELETE FROM user_roles');

    db2.exec('DELETE FROM sessions');
  });

  test('test login page', async () => {
    const c = new ControllerAuth({ body: 'username=test&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('login');
    if (r.status === 500 || r.status === 404)console.log(c.error);
  });

  test('display login fail', async () => {
    const c = new ControllerAuth({ body: 'username=hellox&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('fail');
    expect(r.status).toBe(200);
  });
});
