const {ORM} = require('kohanajs');

class Role extends ORM{
  name = null;

  static joinTablePrefix = 'role';
  static tableName = 'roles';

  static fields = new Map([
    ["name", "String"]
  ]);
}

module.exports = Role;
