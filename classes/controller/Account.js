const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, ControllerMixinMime, ControllerMixinView, KohanaJS, ORM } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { ControllerMixinSession } = require('@kohanajs/mod-session');

const ControllerMixinLoginRequire = require('../controller-mixin/LoginRequire');
const ControllerMixinAccount = require("../controller-mixin/Account");
const User = ORM.require('User');

class ControllerAccount extends Controller {
  static mixins = [...Controller.mixins,
    ControllerMixinDatabase,
    ControllerMixinSession,
    ControllerMixinLoginRequire,
    ControllerMixinMultipartForm,
    ControllerMixinMime,
    ControllerMixinView,
    ControllerMixinAccount
  ]

  /**
   *
   * @param request
   * @param opts
   * @param opts.databaseMap
   * @param opts.allowRoles
   * @param opts.rejectLanding
   * @param opts.layout
   */
  constructor(request, opts = {}) {
    super(request);

    const {
      databaseMap = new Map([
        ['session', `${KohanaJS.config.auth.databasePath}/session.sqlite`],
        ['admin', `${KohanaJS.config.auth.databasePath}/${KohanaJS.config.auth.userDatabase}`],
      ]),
      allowRoles = ['*'],
      rejectLanding = '/login',
      layout = 'layout/account',
    } = opts;

    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', databaseMap.get('session'))
      .set('admin', databaseMap.get('admin'));

    this.state.set(ControllerMixinView.LAYOUT_FILE, layout);
    this.state.set(ControllerMixinLoginRequire.REJECT_LANDING, rejectLanding);
    this.state.set(ControllerMixinLoginRequire.ALLOW_ROLES, new Set(allowRoles));
  }

  async before() {
    this.state.get(ControllerMixinView.LAYOUT).data.user_role = this.request.session.user_role;
  }

  async action_index() {
    const userId = this.request.session.user_id;
    const database = this.state.get(ControllerMixinDatabase.DATABASES).get('admin');
    const user = await ORM.factory(User, userId, {database});
    const person = await user.parent('person_id');

    const identifiers = {};

    await Promise.all(KohanaJS.config.auth.identifiers.map(async it => {
      identifiers[it.name] = await ORM.readBy(it.Model, 'user_id', [parseInt(userId)], {database, asArray: true});
    }));

    this.setTemplate('templates/account/dashboard', {
      person,
      identifiers,
      activated: !!user.activated
    });
  }

  async action_change_person(){}
  async action_action_change_person_post(){}
}

module.exports = ControllerAccount;
