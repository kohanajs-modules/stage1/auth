/* Controller auth handle login, logout */
const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, ControllerMixinMime, ControllerMixinView, KohanaJS, ORM} = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { ControllerMixinSession } = require('@kohanajs/mod-session');

const ControllerMixinAuth = require('../controller-mixin/Auth');
const ControllerMixinRegister = require('../controller-mixin/Register');
const HelperAuth = require('../helper/Auth');

class ControllerRegister extends Controller {
  static mixins = [...Controller.mixins,
    ControllerMixinMultipartForm,
    ControllerMixinDatabase,
    ControllerMixinSession,
    ControllerMixinMime,
    ControllerMixinView,
    ControllerMixinRegister
  ];

  constructor(request) {
    super(request);

    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', `${KohanaJS.config.auth.databasePath}/session.sqlite`)
      .set('admin', `${KohanaJS.config.auth.databasePath}/${KohanaJS.config.auth.userDatabase}`);
  }

  async action_register_post() {
    const postData = this.state.get(ControllerMixinMultipartForm.POST_DATA);
    //assign login object to session
    await HelperAuth.do_login(this.state, this.state.get(ControllerMixinAuth.USER));
    await HelperAuth.redirect(this.state, postData.destination);
  }
}

module.exports = ControllerRegister;
